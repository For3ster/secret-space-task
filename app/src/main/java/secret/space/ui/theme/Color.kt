package secret.space.ui.theme

import androidx.compose.ui.graphics.Color

val Primary = Color(0xFF000000)
val Secondary = Color(0xFF808080)
val BackgroundLight = Color(0xFFECECEC)
val BackgroundDark = Color(0xFF141414)
val SurfaceLight = Color(0xFFCACACA)
val SurfaceDark = Color(0xFF313131)