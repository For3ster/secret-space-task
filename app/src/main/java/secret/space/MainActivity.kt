package secret.space

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import dagger.hilt.android.AndroidEntryPoint
import secret.space.feature_launch_details.navigation.launchDetailsScreen
import secret.space.feature_launch_details.navigation.navigateToLaunchDetails
import secret.space.feature_launches.navigation.launchesRoute
import secret.space.feature_launches.navigation.launchesScreen
import secret.space.ui.theme.SecretSpaceTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SecretSpaceTheme {
                val navController: NavHostController = rememberNavController()
                NavHost(navController = navController, startDestination = launchesRoute) {
                    launchesScreen(
                        onLaunchClick = navController::navigateToLaunchDetails
                    )
                    launchDetailsScreen(
                        onBackClick = navController::popBackStack
                    )
                }
            }
        }
    }
}
