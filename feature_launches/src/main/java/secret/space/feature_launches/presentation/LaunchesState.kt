package secret.space.feature_launches.presentation

import com.airbnb.mvrx.MavericksState
import secret.space.LaunchesQuery

internal data class LaunchesState(
    val isLoading: Boolean = false,
    val isNextPageLoading: Boolean = false,
    val cursor: String? = null,
    val launchesResult: Result<LaunchesQuery.Launches>? = null,
    val launches: List<LaunchesQuery.Launch> = emptyList(),
) : MavericksState {

    val hasNextPage: Boolean
        get() = cursor == null || launchesResult?.getOrNull()?.hasMore == true
}
