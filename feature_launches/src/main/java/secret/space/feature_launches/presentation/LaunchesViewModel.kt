package secret.space.feature_launches.presentation

import com.airbnb.mvrx.MavericksViewModel
import com.airbnb.mvrx.MavericksViewModelFactory
import com.airbnb.mvrx.hilt.AssistedViewModelFactory
import com.airbnb.mvrx.hilt.hiltMavericksViewModelFactory
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import secret.space.LaunchesQuery
import secret.space.domain_launches.LaunchesRepository

internal class LaunchesViewModel @AssistedInject constructor(
    @Assisted initialState: LaunchesState,
    private val repository: LaunchesRepository,
) : MavericksViewModel<LaunchesState>(initialState) {

    suspend fun loadLaunches() {
        val state = awaitState()
        if (!state.hasNextPage) return

        setState {
            if (cursor == null) {
                copy(isLoading = true)
            } else {
                copy(isNextPageLoading = true)
            }
        }
        val result = repository.getLaunches(state.cursor)
        val newCursor = if (result.getOrNull()?.hasMore == true) {
            result.getOrNull()?.cursor
        } else null

        setState {
            copy(
                isLoading = false,
                isNextPageLoading = false,
                launchesResult = result,
                launches = this.launches + result.getLaunchListOrEmpty(),
                cursor = newCursor ?: this.cursor
            )
        }
    }

    @AssistedFactory
    interface Factory : AssistedViewModelFactory<LaunchesViewModel, LaunchesState> {
        override fun create(state: LaunchesState): LaunchesViewModel
    }

    companion object :
        MavericksViewModelFactory<LaunchesViewModel, LaunchesState> by hiltMavericksViewModelFactory()
}

private fun Result<LaunchesQuery.Launches>.getLaunchListOrEmpty() =
    getOrNull()?.launches?.filterNotNull().orEmpty()
