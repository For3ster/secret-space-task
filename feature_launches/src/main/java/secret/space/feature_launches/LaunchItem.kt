package secret.space.feature_launches

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import secret.space.LaunchesQuery

@OptIn(ExperimentalMaterial3Api::class)
@Composable
internal fun LaunchItem(
    item: LaunchesQuery.Launch,
    onItemClick: (String) -> Unit,
) {
    ElevatedCard(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(top = 8.dp, bottom = 8.dp),
        onClick = { onItemClick(item.id) }
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxSize()
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                AsyncImage(
                    model = item.mission?.missionPatch,
                    contentDescription = null,
                    modifier = Modifier.size(70.dp)
                )
                Column(
                    modifier = Modifier
                        .padding(start = 16.dp)
                        .fillMaxSize(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    item.rocket?.name?.let { Rocket(name = it) }
                    item.mission?.name?.let { Mission(name = it) }
                }
            }
        }
    }
}

@Composable
private fun Rocket(name: String) {
    Text(
        modifier = Modifier.fillMaxWidth(),
        text = name,
        style = MaterialTheme.typography.titleLarge,
        overflow = TextOverflow.Ellipsis,
        maxLines = 1
    )
}

@Composable
private fun Mission(name: String) {
    Text(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 4.dp),
        text = name,
        style = MaterialTheme.typography.bodyMedium.copy(
            letterSpacing = 1.5.sp
        ),
        overflow = TextOverflow.Ellipsis,
        maxLines = 2
    )
}
