package secret.space.feature_launches

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.LargeTopAppBar
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.airbnb.mvrx.compose.collectAsState
import com.airbnb.mvrx.compose.mavericksViewModel
import secret.space.feature_launches.presentation.LaunchesState
import secret.space.feature_launches.presentation.LaunchesViewModel

@Composable
internal fun LaunchesRoute(
    onLaunchClick: (String) -> Unit,
    viewModel: LaunchesViewModel = mavericksViewModel()
) {
    val state by viewModel.collectAsState()
    LaunchesScreen(
        state = state,
        onListScrolledToEnd = viewModel::loadLaunches,
        onLaunchClick = onLaunchClick,
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
internal fun LaunchesScreen(
    state: LaunchesState,
    onListScrolledToEnd: suspend () -> Unit,
    onLaunchClick: (String) -> Unit,
) {

    val listState = rememberLazyListState()
    val listScrolledToEnd by remember {
        derivedStateOf {
            listState.isListCloseToEnd()
        }
    }
    val scrollBehavior =
        TopAppBarDefaults.exitUntilCollapsedScrollBehavior(rememberTopAppBarState())

    Scaffold(
        modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
        topBar = {
            LargeTopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.background,
                ),
                title = {
                    Text(stringResource(R.string.launches_title))
                },
                scrollBehavior = scrollBehavior
            )
        }
    ) { innerPadding ->
        LaunchedEffect(listScrolledToEnd) {
            if (listScrolledToEnd) onListScrolledToEnd()
        }

        when {
            state.isLoading -> {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                ) {
                    CircularProgressIndicator(
                        modifier = Modifier.align(Alignment.Center)
                    )
                }
            }

            state.launchesResult?.isFailure == true -> {
                Text(
                    text = state.launchesResult.exceptionOrNull().toString(),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(
                            top = innerPadding.calculateTopPadding(),
                            start = 16.dp,
                            end = 16.dp
                        )
                )
            }

            state.launchesResult?.isSuccess == true -> {
                LaunchList(
                    state = state,
                    innerPadding = innerPadding,
                    listState = listState,
                    onLaunchClick = onLaunchClick
                )
            }
        }
    }
}

@Composable
private fun LaunchList(
    state: LaunchesState,
    innerPadding: PaddingValues,
    listState: LazyListState,
    onLaunchClick: (String) -> Unit,
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                top = innerPadding.calculateTopPadding(),
                start = 16.dp,
                end = 16.dp
            ),
        state = listState,
    ) {
        items(state.launches) { launch ->
            LaunchItem(item = launch, onItemClick = onLaunchClick)
        }
        if (state.isNextPageLoading) {
            item {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 8.dp)
                ) {
                    CircularProgressIndicator(
                        strokeWidth = 2.dp,
                        modifier = Modifier
                            .align(Alignment.Center)
                            .size(24.dp)
                    )
                }
            }
        }
    }
}

private fun LazyListState.isListCloseToEnd() =
    (layoutInfo.visibleItemsInfo.lastOrNull()?.index ?: 0) > layoutInfo.totalItemsCount - 3
