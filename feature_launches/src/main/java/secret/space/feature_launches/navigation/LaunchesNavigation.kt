package secret.space.feature_launches.navigation

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import secret.space.feature_launches.LaunchesRoute

const val launchesRoute = "launches_route"

fun NavGraphBuilder.launchesScreen(
    onLaunchClick: (String) -> Unit,
) {
    composable(route = launchesRoute) {
        LaunchesRoute(onLaunchClick)
    }
}
