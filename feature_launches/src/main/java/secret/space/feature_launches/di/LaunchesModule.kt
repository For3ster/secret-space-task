package secret.space.feature_launches.di

import com.airbnb.mvrx.hilt.AssistedViewModelFactory
import com.airbnb.mvrx.hilt.MavericksViewModelComponent
import com.airbnb.mvrx.hilt.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.multibindings.IntoMap
import secret.space.feature_launches.presentation.LaunchesViewModel

@Module
@InstallIn(MavericksViewModelComponent::class)
internal interface LaunchesModule {

    @Binds
    @IntoMap
    @ViewModelKey(LaunchesViewModel::class)
    fun launchesViewModelFactory(factory: LaunchesViewModel.Factory): AssistedViewModelFactory<*, *>
}
