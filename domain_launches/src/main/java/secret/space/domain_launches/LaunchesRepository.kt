package secret.space.domain_launches

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.ApolloResponse
import com.apollographql.apollo3.api.Operation
import com.apollographql.apollo3.api.Optional
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import secret.space.LaunchDetailsQuery
import secret.space.LaunchesQuery
import javax.inject.Inject

class LaunchesRepository @Inject constructor(
    private val apollo: ApolloClient
) {
    suspend fun getLaunches(cursor: String?): Result<LaunchesQuery.Launches> =
        withContext(Dispatchers.IO) {
            try {
                val response = apollo.query(LaunchesQuery(Optional.present(cursor))).execute()
                if (response.hasErrors()) {
                    response.toResultFailure()
                } else {
                    Result.success(response.dataAssertNoErrors.launches)
                }
            } catch (e: Exception) {
                Result.failure(e)
            }
        }
    suspend fun getLaunchDetails(id: String): Result<LaunchDetailsQuery.Launch> =
        withContext(Dispatchers.IO) {
            try {
                val response = apollo.query(LaunchDetailsQuery(launchId = id)).execute()
                when {
                    response.hasErrors() -> {
                        response.toResultFailure()
                    }

                    response.dataAssertNoErrors.launch == null -> {
                        Result.failure(Exception("No launch details"))
                    }

                    else -> {
                        Result.success(response.dataAssertNoErrors.launch!!)
                    }
                }
            } catch (e: Exception) {
                Result.failure(e)
            }
        }
}
private fun <T, D : Operation.Data> ApolloResponse<D>.toResultFailure(): Result<T> {
    val errorMessage = errors?.first()?.message.orEmpty()
    return Result.failure(Exception(errorMessage))
}
