package secret.space.common_network.di

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.network.okHttpClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import secret.space.common_network.NetworkConfig
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val SERVER_URL = "https://apollo-fullstack-tutorial.herokuapp.com/graphql"

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
            .apply { level = HttpLoggingInterceptor.Level.BODY }

        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .readTimeout(NetworkConfig.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(NetworkConfig.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(NetworkConfig.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun provideApolloClient(client: OkHttpClient): ApolloClient =
        ApolloClient.Builder()
            .okHttpClient(client)
            .serverUrl(SERVER_URL)
            .build()
}
