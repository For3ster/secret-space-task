# Secret Escapes | Space Coding Task

The decisions made in this project were driven by the original requirements and were intended to provide an example of the implementation of a potentially scalable project.

To build the architecture inside the module, Mavericks was chosen, which is a handy MVI Android framework from Airbnb.
This is one of many potential options, but it was interesting to show a variation of MVI that has a fairly low entry threshold and is simply supported by developers on a potentially large and scalable project.

### Tech info

  - [Kotlin](https://kotlinlang.org)
  - [Compose](https://developer.android.com/jetpack/compose)
  - MVI Architecture
  - Min SDK 24

### Libraries Used

* [Hilt](https://developer.android.com/training/dependency-injection/hilt-android) for dependency injection
* [OkHttp](https://github.com/square/okhttp) as an Http client
* [Apollo](https://www.apollographql.com/docs/kotlin/) as a GraphQL client
* [Mavericks](https://airbnb.io/mavericks/#/) - Android MVI framework
* [Navigation component](https://developer.android.com/jetpack/compose/navigation) as a compose navigation library
* [Coil](https://coil-kt.github.io/coil/) for image loading
