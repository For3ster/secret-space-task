package secret.space.feature_launch_details.di

import com.airbnb.mvrx.hilt.AssistedViewModelFactory
import com.airbnb.mvrx.hilt.MavericksViewModelComponent
import com.airbnb.mvrx.hilt.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.multibindings.IntoMap
import secret.space.feature_launch_details.presentation.LaunchDetailsViewModel

@Module
@InstallIn(MavericksViewModelComponent::class)
internal interface LaunchDetailsModule {

    @Binds
    @IntoMap
    @ViewModelKey(LaunchDetailsViewModel::class)
    fun launchDetailsViewModelFactory(factory: LaunchDetailsViewModel.Factory): AssistedViewModelFactory<*, *>
}
