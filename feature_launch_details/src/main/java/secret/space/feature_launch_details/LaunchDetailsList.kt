package secret.space.feature_launch_details

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import secret.space.LaunchDetailsQuery

@Composable
internal fun LaunchDetailsList(
    launch: LaunchDetailsQuery.Launch,
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp)
    ) {
        launch.rocket?.let {
            item { Rocket(rocket = it) }
        }
        launch.mission?.let {
            item { Mission(mission = it) }
        }
        launch.site?.let {
            item { Site(site = it) }
        }
    }
}

@Composable
private fun Rocket(rocket: LaunchDetailsQuery.Rocket) {
    Column(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = stringResource(R.string.rocket),
            style = titleStyle(),
        )
        Text(
            text = "ID: ${rocket.id}",
            style = bodyStyle(),
        )
        Text(
            text = "NAME: ${rocket.name}",
            style = bodyStyle(),
        )
        Text(
            text = "TYPE: ${rocket.type}",
            style = bodyStyle(),
        )
    }
}

@Composable
private fun Mission(mission: LaunchDetailsQuery.Mission) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp)
    ) {
        Text(
            text = stringResource(R.string.mission),
            style = titleStyle(),
        )
        Text(
            text = "NAME: ${mission.name}",
            style = bodyStyle(),
        )
    }
}

@Composable
private fun Site(site: String) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp)
    ) {
        Text(
            text = stringResource(R.string.site),
            style = titleStyle(),
        )
        Text(
            text = site,
            style = bodyStyle(),
        )
    }
}

@Composable
private fun titleStyle(): TextStyle =
    MaterialTheme.typography.titleLarge.copy(
        fontSize = 24.sp,
        fontWeight = FontWeight.Bold
    )

@Composable
private fun bodyStyle(): TextStyle =
    MaterialTheme.typography.bodyMedium.copy(fontSize = 16.sp)
