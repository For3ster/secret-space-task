package secret.space.feature_launch_details.navigation

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import secret.space.feature_launch_details.LaunchDetailsRoute

const val LAUNCH_ID_ARG = "LaunchIdArg"
const val launchDetailsRoute = "launch_details_route"

fun NavController.navigateToLaunchDetails(launchId: String) {
    this.navigate("$launchDetailsRoute/$launchId")
}

fun NavGraphBuilder.launchDetailsScreen(onBackClick: () -> Unit) {
    composable(route = "$launchDetailsRoute/{$LAUNCH_ID_ARG}") { navBackStackEntry ->
        navBackStackEntry.arguments?.getString(LAUNCH_ID_ARG)?.let {
            LaunchDetailsRoute(
                launchId = it,
                onBackClick = onBackClick,
            )
        }
    }
}
