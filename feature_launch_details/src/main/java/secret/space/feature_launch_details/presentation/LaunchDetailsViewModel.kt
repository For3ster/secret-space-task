package secret.space.feature_launch_details.presentation

import com.airbnb.mvrx.MavericksViewModel
import com.airbnb.mvrx.MavericksViewModelFactory
import com.airbnb.mvrx.hilt.AssistedViewModelFactory
import com.airbnb.mvrx.hilt.hiltMavericksViewModelFactory
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import secret.space.domain_launches.LaunchesRepository

internal class LaunchDetailsViewModel @AssistedInject constructor(
    @Assisted initialState: LaunchDetailsState,
    private val repository: LaunchesRepository,
) : MavericksViewModel<LaunchDetailsState>(initialState) {

    suspend fun loadLaunchDetails(launchId: String) {
        setState { copy(isLoading = true) }
        val result = repository.getLaunchDetails(launchId)
        setState { copy(isLoading = false, launchResult = result) }
    }

    @AssistedFactory
    interface Factory : AssistedViewModelFactory<LaunchDetailsViewModel, LaunchDetailsState> {
        override fun create(state: LaunchDetailsState): LaunchDetailsViewModel
    }

    companion object :
        MavericksViewModelFactory<LaunchDetailsViewModel, LaunchDetailsState> by hiltMavericksViewModelFactory()
}
