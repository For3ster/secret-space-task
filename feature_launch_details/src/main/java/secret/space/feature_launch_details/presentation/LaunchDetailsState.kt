package secret.space.feature_launch_details.presentation

import com.airbnb.mvrx.MavericksState
import secret.space.LaunchDetailsQuery

internal data class LaunchDetailsState(
    val isLoading: Boolean = false,
    val launchResult: Result<LaunchDetailsQuery.Launch>? = null,
) : MavericksState
