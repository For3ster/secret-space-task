package secret.space.feature_launch_details

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.airbnb.mvrx.compose.collectAsState
import com.airbnb.mvrx.compose.mavericksViewModel
import secret.space.feature_launch_details.presentation.LaunchDetailsState
import secret.space.feature_launch_details.presentation.LaunchDetailsViewModel

@Composable
internal fun LaunchDetailsRoute(
    launchId: String,
    onBackClick: () -> Unit,
    viewModel: LaunchDetailsViewModel = mavericksViewModel()
) {
    val state by viewModel.collectAsState()
    LaunchDetailsScreen(
        state = state,
        launchId = launchId,
        onScreenInit = { viewModel.loadLaunchDetails(launchId) },
        onBackClick = onBackClick,
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
internal fun LaunchDetailsScreen(
    state: LaunchDetailsState,
    launchId: String,
    onScreenInit: suspend () -> Unit,
    onBackClick: () -> Unit,
) {
    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.background,
                ),
                title = {
                    Text(stringResource(R.string.launch_details).format(launchId))
                },
                navigationIcon = {
                    IconButton(onClick = onBackClick) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = null
                        )
                    }
                }
            )
        }
    ) { innerPadding ->
        LaunchedEffect(Unit) {
            onScreenInit()
        }

        when {
            state.isLoading -> {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                ) {
                    CircularProgressIndicator(
                        modifier = Modifier.align(Alignment.Center)
                    )
                }
            }

            state.launchResult?.isFailure == true -> {
                Text(
                    text = state.launchResult.exceptionOrNull().toString(),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(
                            top = innerPadding.calculateTopPadding(),
                            start = 16.dp,
                            end = 16.dp
                        )
                )
            }

            state.launchResult?.isSuccess == true -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(
                            top = innerPadding.calculateTopPadding(),
                            start = 16.dp,
                            end = 16.dp
                        )
                ) {
                    state.launchResult.getOrNull()?.mission?.missionPatch?.let { path ->
                        AsyncImage(
                            model = path,
                            contentDescription = null,
                            modifier = Modifier
                                .align(Alignment.CenterHorizontally)
                                .fillMaxWidth()
                        )
                    }
                    LaunchDetailsList(
                        launch = state.launchResult.getOrThrow(),
                    )
                }
            }
        }
    }
}
